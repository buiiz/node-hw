const { Router } = require("express");
const FighterController = require("../controllers/fighterController");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

router.get("/", FighterController.getAll, responseMiddleware);
router.get("/:id", FighterController.getOne, responseMiddleware);
router.post(
  "/",
  createFighterValid,
  FighterController.create,
  responseMiddleware
);
router.put(
  "/:id",
  updateFighterValid,
  FighterController.update,
  responseMiddleware
);
router.delete("/:id", FighterController.delete, responseMiddleware);

module.exports = router;
