const { Router } = require("express");
const UserController = require("../controllers/userController");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get("/", UserController.getAll, responseMiddleware);
router.get("/:id", UserController.getOne, responseMiddleware);
router.post("/", createUserValid, UserController.create, responseMiddleware);
router.put("/:id", updateUserValid, UserController.update, responseMiddleware);
router.delete("/:id", UserController.delete, responseMiddleware);

module.exports = router;
