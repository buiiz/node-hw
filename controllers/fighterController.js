const FighterService = require("../services/fighterService");

class FighterController {
  getAll(req, res, next) {
    try {
      const fighters = FighterService.getAll();
      res.data = fighters;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }

  getOne(req, res, next) {
    try {
      const fighter = FighterService.getOne({ id: req.params.id });
      res.data = fighter;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }

  create(req, res, next) {
    try {
      const fighter = FighterService.create(req.body);
      res.data = fighter;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }

  update(req, res, next) {
    try {
      const updatedFighter = FighterService.update(req.params.id, req.body);
      res.data = updatedFighter;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }

  delete(req, res, next) {
    try {
      const fighter = FighterService.delete(req.params.id);
      res.data = fighter;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }
}

module.exports = new FighterController();
