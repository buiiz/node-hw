const UserService = require("../services/userService");

class UserController {
  getAll(req, res, next) {
    try {
      const users = UserService.getAll();
      res.data = users;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }

  getOne(req, res, next) {
    try {
      const user = UserService.getOne({ id: req.params.id });
      res.data = user;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }

  create(req, res, next) {
    try {
      const user = UserService.create(req.body);
      res.data = user;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }

  update(req, res, next) {
    try {
      const updatedUser = UserService.update(req.params.id, req.body);
      res.data = updatedUser;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }

  delete(req, res, next) {
    try {
      const user = UserService.delete(req.params.id);
      res.data = user;
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  }
}

module.exports = new UserController();
