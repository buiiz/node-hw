const NotFoundError = require("../errors/NotFoundError");
const ValidationError = require("../errors/ValidationError");

const createErrorResponse = (message) => ({
  error: true,
  message: message,
});

const responseMiddleware = (req, res, next) => {
  if (res.error) {
    switch (true) {
      case res.error instanceof NotFoundError:
        res.status(404).json(createErrorResponse(res.error.message));
        break;
      case res.error instanceof ValidationError:
        res.status(400).json(createErrorResponse(res.error.message));
        break;
      default:
        res.status(400).json(createErrorResponse(res.error.message));
        break;
    }
  } else if (res.data) {
    res.status(200).json(res.data);
  }

  next();
};

exports.responseMiddleware = responseMiddleware;
