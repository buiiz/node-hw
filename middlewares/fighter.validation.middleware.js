const { fighter: fighterModel } = require("../models/fighter");
const ValidationError = require("../errors/ValidationError");
const NotFoundError = require("../errors/NotFoundError");
const FighterService = require("../services/fighterService");

const createFighterValid = (req, res, next) => {
  try {
    const fighter = {
      name: req.body.name,
      health: req.body.health ? req.body.health : 100,
      power: req.body.power,
      defense: req.body.defense,
    };

    switch (true) {
      case isFighterNameExists(fighter.name):
        throw new ValidationError("Fighter with the name already exists");
      case !isValidName(fighter.name):
        throw new ValidationError("Name validation error");
      case !isValidHealth(fighter.health):
        throw new ValidationError("Health validation error");
      case !isValidPower(fighter.power):
        throw new ValidationError("Power validation error");
      case !isValidDefence(fighter.defense):
        throw new ValidationError("Defense validation error");
      case hasRedundantData(req.body):
        throw new ValidationError("Fighter entity isn't valid");
      default:
        req.body = fighter;
        next();
    }
  } catch (error) {
    res.error = error;
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  try {
    const fighter = {
      id: req.params.id,
      name: req.body.name,
      health: req.body.health ? req.body.health : 100,
      power: req.body.power,
      defense: req.body.defense,
    };

    switch (true) {
      case !isFighterExists(fighter.id):
        throw new NotFoundError("Fighter not found");
      case isFighterNameExists(fighter.name):
        throw new ValidationError("Fighter with the name already exists");
      case !isValidName(fighter.name):
        throw new ValidationError("Name validation error");
      case !isValidHealth(fighter.health):
        throw new ValidationError("Health validation error");
      case !isValidPower(fighter.power):
        throw new ValidationError("Power validation error");
      case !isValidDefence(fighter.defense):
        throw new ValidationError("Defense validation error");
      case hasRedundantData(req.body):
        throw new ValidationError("Fighter entity isn't valid");
      default:
        req.body = fighter;
        next();
    }
  } catch (error) {
    res.error = error;
    next();
  }
};

const isValidName = (name) => String(name).length > 0;

const isValidHealth = (health) => {
  return Number.isInteger(health) && +health > 1 && +health < 120;
};

const isValidPower = (power) => {
  return Number.isInteger(power) && +power > 1 && +power < 100;
};

const isValidDefence = (defense) => {
  return Number.isInteger(defense) && +defense > 1 && +defense < 10;
};

const hasRedundantData = (fighter) => {
  if (fighter.id) return true;
  return !Object.keys(fighter).every((key) => key in fighterModel);
};

const isFighterExists = (id) => !!FighterService.getOne({ id });

const isFighterNameExists = (name) => {
  return !!FighterService.getOne({ name });
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
