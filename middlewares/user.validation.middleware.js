const { user: userModel } = require("../models/user");
const ValidationError = require("../errors/ValidationError");
const NotFoundError = require("../errors/NotFoundError");
const UserService = require("../services/userService");

const createUserValid = (req, res, next) => {
  try {
    const user = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      phoneNumber: req.body.phoneNumber,
      password: req.body.password,
    };

    switch (true) {
      case isUserEmailExists(user.email):
        throw new ValidationError("User with the email already exists");
      case isUserPhoneExists(user.phoneNumber):
        throw new ValidationError("User with the phone number already exists");
      case !isValidName(user.firstName):
        throw new ValidationError("First name validation error");
      case !isValidName(user.lastName):
        throw new ValidationError("Last name validation error");
      case !isValidPassword(user.password):
        throw new ValidationError("Password validation error");
      case !isValidEmail(user.email):
        throw new ValidationError("Email validation error");
      case !isValidPhone(user.phoneNumber):
        throw new ValidationError("Phone number validation error");
      case hasRedundantData(req.body):
        throw new ValidationError("User entity isn't valid");
      default:
        req.body = user;
        next();
    }
  } catch (error) {
    res.error = error;
    next();
  }
};

const updateUserValid = (req, res, next) => {
  try {
    const user = {
      id: req.params.id,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      phoneNumber: req.body.phoneNumber,
      password: req.body.password,
    };

    switch (true) {
      case !isUserExists(user.id):
        throw new NotFoundError("User not found");
      case isUserEmailExists(user.email):
        throw new ValidationError("User with the email already exists");
      case isUserPhoneExists(user.phoneNumber):
        throw new ValidationError("User with the phone number already exists");
      case !isValidName(user.firstName):
        throw new ValidationError("First name validation error");
      case !isValidName(user.lastName):
        throw new ValidationError("Last name validation error");
      case !isValidPassword(user.password):
        throw new ValidationError("Password validation error");
      case !isValidEmail(user.email):
        throw new ValidationError("Email validation error");
      case !isValidPhone(user.phoneNumber):
        throw new ValidationError("Phone number validation error");
      case hasRedundantData(req.body):
        throw new ValidationError("User entity isn't valid");
      default:
        req.body = user;
        next();
    }
  } catch (error) {
    res.error = error;
    next();
  }
};

const isValidName = (name) => String(name).length > 0;

const isValidPassword = (password) => String(password).length >= 3;

const isValidEmail = (email) => {
  return /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/.test(email);
};

const isValidPhone = (phone) => /^\+380[0-9]{9}$/.test(phone);

const hasRedundantData = (user) => {
  if (user.id) return true;
  return !Object.keys(user).every((key) => key in userModel);
};

const isUserExists = (id) => !!UserService.getOne({ id });

const isUserEmailExists = (email) => {
  return !!UserService.getOne({ email });
};
const isUserPhoneExists = (phoneNumber) => {
  return !!UserService.getOne({ phoneNumber });
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
