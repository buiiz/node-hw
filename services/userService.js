const { UserRepository } = require("../repositories/userRepository");

class UserService {
  getAll() {
    const users = UserRepository.getAll();
    return users;
  }

  getOne(search) {
    const user = UserRepository.getOne(search);
    return user;
  }

  create(data) {
    const createdUser = UserRepository.create(data);
    return createdUser;
  }

  update(id, data) {
    const updatedUser = UserRepository.update(id, data);
    return updatedUser;
  }

  delete(id) {
    const removedUser = UserRepository.delete(id);
    return removedUser;
  }
}

module.exports = new UserService();
