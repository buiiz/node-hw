const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  getAll() {
    const fighters = FighterRepository.getAll();
    return fighters;
  }

  getOne(search) {
    const fighter = FighterRepository.getOne(search);
    return fighter;
  }

  create(data) {
    const createdFighter = FighterRepository.create(data);
    return createdFighter;
  }

  update(id, data) {
    const updatedFighter = FighterRepository.update(id, data);
    return updatedFighter;
  }

  delete(id) {
    const removedFighter = FighterRepository.delete(id);
    return removedFighter;
  }
}

module.exports = new FighterService();
